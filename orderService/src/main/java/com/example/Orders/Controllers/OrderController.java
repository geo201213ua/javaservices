package com.example.Orders.Controller;

import com.example.Orders.Models.OrderObjects.OrderObject;
import com.example.Orders.Service.KafkaProducerService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

    @Autowired
    KafkaProducerService kafkaProducerService;

    @PostMapping(path="/create", consumes = "application/json", produces = "application/json")
    public String create(@RequestBody String model) {
        try {
            JSONObject json = new JSONObject(model);
            OrderObject orderObject = new OrderObject();
            orderObject.putJson(json);
            kafkaProducerService.reserveItems(model);
            return "Successfully added";
        } catch (Exception e) {
            return getErrorMessage(e);
        }
    }

    @PostMapping(path="/get", consumes = "application/json", produces = "application/json")
    public String get(@RequestBody String model) {
        try {
            JSONObject json = new JSONObject(model);
            var id = getIdFrom(json);
            OrderObject orderObject = new OrderObject();
            return orderObject.getJsonItems(id).toString();
        } catch (Exception e) {
            return getErrorMessage(e);
        }
    }

    @PostMapping(path="/payout", consumes = "application/json", produces = "application/json")
    public String payout(@RequestBody String model) {
        try {
            kafkaProducerService.removeItems(model);
            return "Success";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    //MARK: - Private Methods

    private long getIdFrom(JSONObject json) throws JSONException {
        return json.getLong("id");
    }

    private String getErrorMessage(Exception e) {
        return "Error - " + e.getMessage() + "\n" + "Exception - " + e;
    }
}
