package com.example.Orders.Service;

public final class EnvService {
        //MARK: -  Public Methods
        String getDbHost() {
            return System.getenv("ORDER_SERVICE_POSTGRES_HOST");
        }
        String getDbPort() {
            return System.getenv("ORDER_SERVICE_POSTGRES_PORT");
        }
        String getDbName() {
            return System.getenv("ORDER_SERVICE_POSTGRES_DB");
        }
        String getDbUser() {
            return System.getenv("ORDER_SERVICE_POSTGRES_USER");
        }
        String getDbPassword() {
            return System.getenv("ORDER_SERVICE_POSTGRES_PASSWORD");
        }
    }

