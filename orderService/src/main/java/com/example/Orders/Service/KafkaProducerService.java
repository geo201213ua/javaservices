package com.example.Orders.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducerService {

    // MARK: - Private Properties
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    // MARK: - Public Methods
    public void removeItems(String message) {
        this.kafkaTemplate.send("removeItems", message);
    }
    public void reserveItems(String message) {
        this.kafkaTemplate.send("reservedItems", message);
    }
}
