package com.example.Orders.Models.Shared;

public enum SQLTables {
    order {
        public String toString() {
            return "order_service.order";
        }
    },
    orderItem {
        public String toString() {
            return "order_service.order_item";
        }
    }
}
