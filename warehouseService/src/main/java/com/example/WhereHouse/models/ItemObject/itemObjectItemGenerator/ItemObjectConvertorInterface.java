package com.example.WhereHouse.models.ItemObject.itemObjectItemGenerator;

import com.example.WhereHouse.models.ItemObject.ItemObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public interface ItemObjectConvertorInterface {
    JSONObject getJsonFrom(ItemObject item) throws JSONException;
    JSONArray getJsonFrom(ArrayList<ItemObject> items) throws JSONException;
    ItemObject getFromResultSet(ResultSet resultSet) throws SQLException;
}
