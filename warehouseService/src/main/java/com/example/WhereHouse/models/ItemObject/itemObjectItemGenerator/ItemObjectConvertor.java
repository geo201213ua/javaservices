package com.example.WhereHouse.models.ItemObject.itemObjectItemGenerator;

import com.example.WhereHouse.models.ItemObject.ItemKeysEnum;
import com.example.WhereHouse.models.ItemObject.ItemObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

public class ItemObjectConvertor implements ItemObjectConvertorInterface{

    public ItemObjectConvertor() {}

    public JSONArray getJsonFrom(ArrayList<ItemObject> items) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (ItemObject item : items) {
            jsonArray.put(this.getJsonFrom(item));
        }
        return  jsonArray;
    }

    public JSONObject getJsonFrom(ItemObject item) throws JSONException {
        var json = new JSONObject();
        json.put(ItemKeysEnum.id.name(), item.getId());
        json.put(ItemKeysEnum.name.name(), item.getName());
        json.put(ItemKeysEnum.total_amount.name(), item.getTotalAmount());
        json.put(ItemKeysEnum.booked_amount.name(), item.getBookedAmount());
        json.put(ItemKeysEnum.created_at.name(), item.getCreatedAt().toString());
        json.put(ItemKeysEnum.updated_at.name(), item.getUpdatedAt().toString());
        return json;
    }

    public ItemObject getFromResultSet(ResultSet resultSet) throws SQLException {
        long id = resultSet.getInt(ItemKeysEnum.id.name());
        String name = resultSet.getString(ItemKeysEnum.name.name());
        long totalAmount = resultSet.getInt(ItemKeysEnum.total_amount.name());
        long bookedAmount = resultSet.getInt(ItemKeysEnum.booked_amount.name());
        Timestamp createdAt = resultSet.getTimestamp(ItemKeysEnum.created_at.name());
        Timestamp updatedAt = resultSet.getTimestamp(ItemKeysEnum.updated_at.name());
        return new ItemObject(id, name, totalAmount, bookedAmount, createdAt, updatedAt);
    }
}
