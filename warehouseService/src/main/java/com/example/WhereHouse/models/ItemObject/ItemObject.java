package com.example.WhereHouse.models.ItemObject;

import com.example.WhereHouse.models.ItemObject.itemObjectItemGenerator.ItemObjectConvertor;
import com.example.WhereHouse.models.ItemObject.itemObjectItemGenerator.ItemObjectConvertorInterface;
import com.example.WhereHouse.models.SQLTables;
import com.example.WhereHouse.services.databaseService.DatabaseService;
import com.example.WhereHouse.services.databaseService.DatabaseServiceInterface;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.sql.*;
import java.text.MessageFormat;
import java.util.ArrayList;

public final class ItemObject implements ItemObjectInterface {

    // Object Properties
    private long id;
    private String name;
    private long totalAmount;
    private long bookedAmount;
    private Timestamp createdAt;
    private Timestamp updatedAt;

    // Service Properties
    private String tableName;
    private ItemObjectConvertorInterface itemObjectConvertor;
    private DatabaseServiceInterface databaseService;

    // Public empty constructor
    public ItemObject() {
        this.databaseService = new DatabaseService();
        this.tableName = SQLTables.item.toString();
        this.itemObjectConvertor = new ItemObjectConvertor();
    }

    public long getId() {
        return this.id;
    }
    public long getName() {
        return this.id;
    }
    public long getTotalAmount() {
        return this.totalAmount;
    }
    public long getBookedAmount() {
        return this.bookedAmount;
    }
    public Timestamp getCreatedAt() {
        return this.createdAt;
    }
    public Timestamp getUpdatedAt() {
        return this.updatedAt;
    }


    // Public constructor
    public ItemObject(
            long id,
            String name,
            long totalAmount,
            long bookedAmount,
            Timestamp createdAt,
            Timestamp updatedAt
    ) {
        this.id = id;
        this.name = name;
        this.totalAmount = totalAmount;
        this.bookedAmount = bookedAmount;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    // Public Methods

    public JSONArray getJsonItems() throws JSONException, SQLException {
        return itemObjectConvertor.getJsonFrom(this.getItems());
    }

    public void reserveItems(JSONArray jsonArray) throws JSONException, SQLException {
        var sqlCommand = "";
        ArrayList<String> ids = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            var jsonItem = jsonArray.getJSONObject(i);
            var item = jsonItem.getString("item_id");
            ids.add(item);
        }

        var items = getItemsById(ids);


        for (int i = 0; i < jsonArray.length(); i++) {
            var item = jsonArray.getJSONObject(i);

            var id = item.getLong("item_id");
            var reservedItems = item.getLong("count");

            for (ItemObject itemValForIndex : items) {
                if (itemValForIndex.id == id) {
                    var newBookedValue = itemValForIndex.bookedAmount + reservedItems;
                    var updateCommand = "UPDATE " + SQLTables.item.toString();
                    var setCommand = " SET " + ItemKeysEnum.booked_amount.toString() + " = " + newBookedValue;
                    var whereCommand = " WHERE " + ItemKeysEnum.id.toString() + " = " + id + ";";
                    var command = updateCommand + setCommand + whereCommand;
                    sqlCommand += command;
                }
            }
        }
        databaseService.updateWithCommand(sqlCommand);
    }

    public void removeItems(JSONArray jsonArray) throws JSONException, SQLException {
        var sqlCommand = "";
        ArrayList<String> ids = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            var jsonItem = jsonArray.getJSONObject(i);
            var item = jsonItem.getString("item_id");
            ids.add(item);
        }

        var items = getItemsById(ids);

        for (int i = 0; i < jsonArray.length(); i++) {
            var item = jsonArray.getJSONObject(i);

            var id = item.getLong("item_id");
            var reservedItems = item.get("count");

            for (ItemObject itemValForIndex : items) {
                if (itemValForIndex.id == id) {
                    var totalCount = itemValForIndex.totalAmount - itemValForIndex.bookedAmount;
                    var updateCommand = "UPDATE " + SQLTables.item.toString();
                    var setCommand = " SET " + ItemKeysEnum.total_amount.toString() + " = " + totalCount + ", " + ItemKeysEnum.booked_amount.toString() + " = 0";
                    var whereCommand = " WHERE " + ItemKeysEnum.id.toString() + " = " + id + ";";
                    var command = updateCommand + setCommand + whereCommand;
                    sqlCommand += command;
                }
            }
        }
        databaseService.updateWithCommand(sqlCommand);
    }

    public void putJson(JSONObject json) throws SQLException, JSONException {
        var rawString = "INSERT INTO" +
                " " +
                tableName +
                "(id, name, total_amount, booked_amount, created_at, updated_at)" +
                " " +
                "VALUES ({0}, {1}, {2}, {3}, {4}, {5})";
        try {
            var sql = MessageFormat.format(
                    rawString,
                    json.get(ItemKeysEnum.id.name()),
                    "'" + json.get(ItemKeysEnum.name.name()) + "'",
                    json.get(ItemKeysEnum.total_amount.name()),
                    json.get(ItemKeysEnum.booked_amount.name()),
                    "'" + json.get(ItemKeysEnum.created_at.name()) + "'",
                    "'" + json.get(ItemKeysEnum.updated_at.name()) + "'"
            );
            databaseService.updateWithCommand(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    private ArrayList<ItemObject> getItems() throws SQLException {
        var sqlTask = "select * from" + " " + tableName;
        var result = databaseService.queryCommand(sqlTask);
        var items = new ArrayList<ItemObject>();
        while (result.next()) {
            var item = itemObjectConvertor.getFromResultSet(result);
            items.add(item);
        }
        return items;
    }

    private ArrayList<ItemObject> getItemsById(ArrayList<String> ids) throws SQLException {
        var idsStringForSql = getSqlCommandValuesForIds(ids);
        var sqlTask = "select * from " + tableName + " WHERE id IN " + idsStringForSql;
        var result = databaseService.queryCommand(sqlTask);
        var items = new ArrayList<ItemObject>();
        while (result.next()) {
            var item = itemObjectConvertor.getFromResultSet(result);
            items.add(item);
        }
        return items;
    }

    private String getSqlCommandValuesForIds(ArrayList<String> ids) {
        var returnString = "(";
        for (int i = 0; i < ids.size(); i++) {
            var id = ids.get(i);
            if (i < ids.size() - 1) {
                returnString += id + ",";
            } else {
                returnString += id + ")";
            }
        }
        return returnString;
    }
}
