package com.example.WhereHouse.models.ItemObject;

public enum ItemKeysEnum {
    id, name, total_amount, booked_amount, created_at, updated_at
}
