package com.example.WhereHouse.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLException;

@SpringBootApplication
public class WhereHouseApplication {

	public static void main(String[] args) throws SQLException {
		SpringApplication.run(WhereHouseApplication.class, args);
	}

}
