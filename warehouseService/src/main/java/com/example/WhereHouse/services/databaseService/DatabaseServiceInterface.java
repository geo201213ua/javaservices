package com.example.WhereHouse.services.databaseService;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface DatabaseServiceInterface {
   public int updateWithCommand(String command) throws SQLException;
   public ResultSet queryCommand(String command) throws SQLException;
}
