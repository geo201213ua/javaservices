package com.example.WhereHouse.services.envService;

public final class ENVService implements ENVServiceInterface {

    // Nested
    private enum EnvKeys {
        WAREHOUSE_SERVICE_POSTGRES_HOST,
        WAREHOUSE_SERVICE_POSTGRES_PORT,
        WAREHOUSE_SERVICE_POSTGRES_DB,
        WAREHOUSE_SERVICE_POSTGRES_USER,
        WAREHOUSE_SERVICE_POSTGRES_PASSWORD
    }

    // Public Methods
    public String getDbHost() {
        return System.getenv(EnvKeys.WAREHOUSE_SERVICE_POSTGRES_HOST.name());
    }
    public String getDbPort() {
        return System.getenv(EnvKeys.WAREHOUSE_SERVICE_POSTGRES_PORT.name());
    }
    public String getDbName() {
        return System.getenv(EnvKeys.WAREHOUSE_SERVICE_POSTGRES_DB.name());
    }
    public String getDbUser() {
        return System.getenv(EnvKeys.WAREHOUSE_SERVICE_POSTGRES_USER.name());
    }
    public String getDbPassword() {
        return System.getenv(EnvKeys.WAREHOUSE_SERVICE_POSTGRES_PASSWORD.name());
    }
}
