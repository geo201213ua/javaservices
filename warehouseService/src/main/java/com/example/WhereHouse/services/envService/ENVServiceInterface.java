package com.example.WhereHouse.services.envService;

public interface ENVServiceInterface {
    String getDbHost();
    String getDbPort();
    String getDbName();
    String getDbUser();
    String getDbPassword();
}
