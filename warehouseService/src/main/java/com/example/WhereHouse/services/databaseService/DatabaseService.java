package com.example.WhereHouse.services.databaseService;
import com.example.WhereHouse.services.envService.ENVService;
import com.example.WhereHouse.services.envService.ENVServiceInterface;

import java.sql.*;

public final class DatabaseService implements DatabaseServiceInterface {

    // Private Properties
    private ENVServiceInterface envService;

    // Public constructor
    public DatabaseService() {
        this.envService = new ENVService();
    }

    // Public Methods
    public int updateWithCommand(String command) throws SQLException {
        var connection = this.getDBConnection();
        var statement = connection.createStatement();
        return statement.executeUpdate(command);
    }

    public ResultSet queryCommand(String command) throws SQLException {
        var connection = this.getDBConnection();
        var statement = connection.createStatement();
        return statement.executeQuery(command);
    }

    // Private Methods
    private Connection getDBConnection() throws SQLException {
        var userName = envService.getDbUser();
        var password = envService.getDbPassword();
        var dbName = envService.getDbName();
        var host = envService.getDbHost();
        var port = envService.getDbPort();
        String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbName;
        final Connection connection = DriverManager.getConnection(url, userName, password);
        return connection;
    }
}
