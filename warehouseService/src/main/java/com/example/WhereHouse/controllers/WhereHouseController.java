package com.example.WhereHouse.controllers;

import com.example.WhereHouse.controllers.ControllersSharedMethods.ControllersSharedMethods;
import com.example.WhereHouse.models.ItemObject.ItemObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

@RestController
public class WhereHouseController {

    // MARK: - Services
    ControllersSharedMethods controllersSharedMethods = new ControllersSharedMethods();

    //MARK: - API Methods
    @PostMapping(path="/listItems", consumes = "application/json", produces = "application/json")
    public String listItems(@RequestBody String model) {
        System.out.println(model);
        ItemObject itemObject = new ItemObject();
        try {
            JSONArray jsonArray = itemObject.getJsonItems();
            return jsonArray.toString();
        } catch (Exception e) {
           return this.controllersSharedMethods.getErrorMessage(e);
        }
    }

    @PostMapping(path="/addItem", consumes = "application/json", produces = "application/json")
    public String addItem(@RequestBody String model) {
        try {
            JSONObject json = new JSONObject(model);
            ItemObject itemObject = new ItemObject();
            itemObject.putJson(json);
            return this.controllersSharedMethods.getSuccessMessage(json);
        } catch (Exception e) {
            return this.controllersSharedMethods.getErrorMessage(e);
        }
    }

    @PostMapping(path="/reserveItems", consumes = "application/json", produces = "application/json")
    public String reserveItems(@RequestBody String model) {
        try {
            var json = new JSONObject(model);
            var jsonArr = json.getJSONArray("arr");
            var newItemObject = new ItemObject();
            newItemObject.reserveItems(jsonArr);
            return "Successfylly added items";
        } catch (Exception e) {
            return controllersSharedMethods.getErrorMessage(e);
        }
    }

    @PostMapping(path="/removeItms", consumes = "application/json", produces = "application/json")
    public String removeItems(@RequestBody String model) {
        try {
            var json = new JSONObject(model);
            var jsonArr = json.getJSONArray("arr");
            var newItemObject = new ItemObject();
            newItemObject.removeItems(jsonArr);
            return "Successfylly added items";
        } catch (Exception e) {
            return controllersSharedMethods.getErrorMessage(e);
        }
    }
}
