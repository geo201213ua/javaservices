package com.example.WhereHouse.controllers.ControllersSharedMethods;

import org.json.JSONObject;

public class ControllersSharedMethods {
    public String getSuccessMessage(JSONObject jsonObject) {
        return jsonObject.toString() + "\n Added successfully";
    }
    public String getErrorMessage(Exception e) {
        return "Error - " + e.getLocalizedMessage() + "\n" +" Class - " + e.getClass();
    }
}
