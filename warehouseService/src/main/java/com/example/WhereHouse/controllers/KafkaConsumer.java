package com.example.WhereHouse.controllers;

import com.example.WhereHouse.models.ItemObject.ItemObject;
import com.example.WhereHouse.models.ItemObject.ItemObjectInterface;
import org.json.JSONObject;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    // MARK: - Private Properties
    private ItemObjectInterface itemObject = new ItemObject();

    // MARK: - Kafka topic handlers
    @KafkaListener(topics="reservedItems",groupId="shitGroup")
    public void consumeFromReserve(String message) {
        try {
            var jsonObject = new JSONObject(message);
            var jsonArr = jsonObject.getJSONArray("arr");
            itemObject.reserveItems(jsonArr);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @KafkaListener(topics="removeItems",groupId="shitGroup")
    public void consumeFromRemoveItems(String message) {
        try {
            var jsonObject = new JSONObject(message);
            var jsonArr = jsonObject.getJSONArray("arr");
            itemObject.removeItems(jsonArr);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
